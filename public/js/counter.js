{
    fetch("/api/counter.json")
        .then(function (response) {
            return response.json();
        })
        .then(function (json) {
            document.getElementById("js-counter").innerHTML = json.count;
        })
        .catch(console.error);
}