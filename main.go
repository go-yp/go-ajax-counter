package main

import (
	"encoding/json"
	"log"
	"net/http"
	"sync/atomic"
)

type CounterResponse struct {
	Count uint32 `json:"count"`
}

func main() {
	var counter = uint32(0)

	http.Handle("/", http.FileServer(http.Dir("./public")))
	http.HandleFunc("/api/counter.json", func(w http.ResponseWriter, _ *http.Request) {
		var newCounter = atomic.AddUint32(&counter, 1)

		json.NewEncoder(w).Encode(&CounterResponse{
			Count: newCounter,
		})
	})

	log.Println("Server running at localhost:8080")

	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal(err)
	}
}
